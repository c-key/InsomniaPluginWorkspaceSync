import { IInsomniaExportResource } from "./insomnia";

export const Plugin = {
  Code: 'workspace-sync',
  Name: 'Workspace Sync'
} as const;

export const Environment = {
  NewLine: "\n"
} as const;

export const ExportTypes = {
  Workspace: 'workspace',
  Environment: 'environment',
  ApiSpec: 'api_spec',
  RequestGroup: 'request_group',
  Request: 'request',
  GrpcRequest: 'grpc_request',
  WebsocketRequest: 'websocket_request',
  WebsocketPayload: 'websocket_payload',
  CookieJar: 'cookie_jar',
  UnitTestSuite: 'unit_test_suite',
  UnitTest: 'unit_test',
  ProtoDirectory: 'proto_directory',
  ProtoFile: 'proto_file'
} as const;

export class Identifier {
  public static isWorkspace = (resource: IInsomniaExportResource) => resource._type === ExportTypes.Workspace;
  public static isEnvironment = (resource: IInsomniaExportResource) => resource._type === ExportTypes.Environment;
  public static isApiSpec = (resource: IInsomniaExportResource) => resource._type === ExportTypes.ApiSpec;
  public static isRequestGroup = (resource: IInsomniaExportResource) => resource._type === ExportTypes.RequestGroup;
  public static isRequest = (resource: IInsomniaExportResource) => resource._type === ExportTypes.Request;
  public static isGrpcRequest = (resource: IInsomniaExportResource) => resource._type === ExportTypes.GrpcRequest;
  public static isWebsocketRequest = (resource: IInsomniaExportResource) => resource._type === ExportTypes.WebsocketRequest;
  public static isWebsocketPayload = (resource: IInsomniaExportResource) => resource._type === ExportTypes.WebsocketPayload;
  public static isCookieJar = (resource: IInsomniaExportResource) => resource._type === ExportTypes.CookieJar;
  public static isUnitTestSuite = (resource: IInsomniaExportResource) => resource._type === ExportTypes.UnitTestSuite;
  public static isUnitTest = (resource: IInsomniaExportResource) => resource._type === ExportTypes.UnitTest;
  public static isProtoDirectory = (resource: IInsomniaExportResource) => resource._type === ExportTypes.ProtoDirectory;
  public static isProtoFile = (resource: IInsomniaExportResource) => resource._type === ExportTypes.ProtoFile;
}