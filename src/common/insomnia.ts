export interface IInsomniaContext {
  app: IInsomniaApp;
  store: IInsomniaStore;
  data: IInsomniaData;
}

export interface IInsomniaApp {
  alert(title: string, message?: string): Promise<void>;
  dialog(title: string, body: HTMLElement, options?: {
    onHide?: () => void;
    tall?: boolean;
    skinny?: boolean;
    wide?: boolean;
  }): void;
  prompt(title: string, options?: {
    label?: string;
    defaultValue?: string;
    submitName?: string;
    cancelable?: boolean;
  }): Promise<string>;
  getInfo(): { version: string, platform: string };
  getPath(name: string): string;
  showSaveDialog(options?: {
    defaultPath?: string;
  }): Promise<string | null>;
  clipboard: {
    readText(): string;
    writeText(text: string): void;
    clear(): void;
  };
}

export interface IInsomniaStore {
  hasItem(key: string): Promise<boolean>;
  getItem(key: string): Promise<string | null>;
  setItem(key: string, value: string): Promise<void>;
  removeItem(key: string): Promise<void>;
  clear(): Promise<void>;
  all(): Promise<Array<{ key: string, value: string }>>;
}

export interface IInsomniaData {
  export: {
    insomnia(options?: {
      includePrivate?: boolean,
      format?: 'json' | 'yaml',
      workspace?: IInsomniaWorkspace,
    }): Promise<string>;
    har(options?: { includePrivate?: boolean }): Promise<string>;
  };
  import: {
    uri(uri: string, options?: ImportOptions): Promise<void>;
    raw(text: string, options?: ImportOptions): Promise<void>;
  };
}

export interface ImportOptions {
  workspaceId?: string;
  workspaceScope?: 'design' | 'collection';
}

export interface IInsomniaModels {
  workspace: IInsomniaWorkspace;
}

export interface IInsomniaWorkspace {
  _id: string;
  name: string;
};

export interface Cookie {
  id: string;
  key: string;
  value: string;
  expires: Date | string | number | null;
  domain: string;
  path: string;
  secure: boolean;
  httpOnly: boolean;
  extensions?: any[];
  creation?: Date;
  creationIndex?: number;
  hostOnly?: boolean;
  pathIsDefault?: boolean;
  lastAccessed?: Date;
}

export interface IInsomniaExport {
  resources: IInsomniaExportResource[];
  _type: string;
  __export_format: number;
  __export_date: string;
  __export_source: string;
}

export interface IInsomniaExportResource {
  _id: string;
  _type: 'workspace' | 'environment' | 'api_spec'
  | 'request_group' | 'request' | 'grpc_request'
  | 'websocket_request' | 'websocket_payload'
  | 'cookie_jar' | 'unit_test_suite' | 'unit_test'
  | 'proto_directory' | 'proto_file';
  body: any;
  name?: string;
  fileName?: string;
  created: number;
  modified: number;
  cookies?: Array<Cookie>;
  scope?: 'design' | 'collection';
}
