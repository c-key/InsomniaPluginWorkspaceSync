import { IInsomniaStore } from '../common/insomnia';
import { Plugin } from '../common/constants';

export default class Config {
  store: IInsomniaStore;
  workspaceId: string;

  constructor(store: IInsomniaStore, workspaceId: string) {
    this.store = store;
    this.workspaceId = workspaceId;
  }

  private configKey = (name: string): string => `${Plugin.Name}:config:${this.workspaceId}:${name}`;

  public Get = async (name: string): Promise<string | null> => this.store.getItem(this.configKey(name));

  public Set = (name: string, value: string): Promise<void> => this.store.setItem(this.configKey(name), value);

  public Has = async (name: string): Promise<boolean> => this.store.hasItem(this.configKey(name));
}
