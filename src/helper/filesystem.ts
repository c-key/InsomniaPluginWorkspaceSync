import AppError from '../common/app-error';

import { mkdir, readdir, readFile, writeFile, unlink } from 'node:fs/promises';

import Path = require('path');

export default class Filesystem {
  public static async CreateFolder(path: string): Promise<void> {
    try {
      await mkdir(path, { recursive: true });
    } catch(e) {
      throw new AppError(`Folder creation failed\n${e}`, e);
    }
  }

  public static async ReadDirectory(path: string): Promise<string[]> {
    try {
      return await readdir(path);
    } catch(e) {
      throw new AppError(`Read Folder content failed\n${e}`, e);
    }
  }

  public static async ReadFile<T>(filename: string, encoding: BufferEncoding = 'utf8') {
    try {
      return JSON.parse(await readFile(filename, encoding)) as T;
    } catch (e) {
      throw new AppError(`Error during reading of file ${filename}\n${e}`, e);
    }
  }

  public static async WriteFile(filename: string, jsonObject: any) {
    try {
      return await writeFile(filename, JSON.stringify(jsonObject, null, 2));
    } catch (e) {
      throw new AppError(`Error during saving of file ${filename}\n${e}`, e);
    }
  }

  public static async RemoveFile(filename: string) {
    try {
      return await unlink(filename);
    } catch (e) {
      throw new AppError(`Error during removing of file ${filename}\n${e}`, e);
    }
  }
}
