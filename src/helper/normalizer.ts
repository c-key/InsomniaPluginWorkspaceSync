import { IInsomniaExport, IInsomniaExportResource } from '../common/insomnia';
import { ExportTypes } from '../common/constants';

export default class Normalizer {
  public static NormalizeExport(oneLineJson: string): IInsomniaExport {
    const content: IInsomniaExport = JSON.parse(oneLineJson);

    content.resources.forEach(r => Normalizer.PrepareResourceForExport(r));
    content.resources.sort(Normalizer.CompareResources);

    return content;
  }

  private static PrepareResourceForExport(resource: IInsomniaExportResource): void {
    Normalizer.ResetTimestamps(resource);
    if (resource._type === ExportTypes.CookieJar) {
      Normalizer.CleanUpCookies(resource);
    }
  }

  private static ResetTimestamps(resource: IInsomniaExportResource): void {
    // Insomnia update the `modified` of a resource even when the resource
    // has only been open in the app. This is very noisy when reviewing
    // changes in requests so `modified` is overwrite with `created` in
    // the exports

    if (resource.modified && resource.created) {
      resource.modified = resource.created;
    }
  }

  private static CleanUpCookies(resource: IInsomniaExportResource): void {
    // Secure cookie should not be sync because they could leak
    // authentication credentials
    resource.cookies = resource.cookies.filter(cookie => !cookie.secure);
  }

  private static CompareResources = (a: IInsomniaExportResource, b: IInsomniaExportResource): number =>
    Normalizer.GetSortKey(a).localeCompare(Normalizer.GetSortKey(b));

  private static GetSortKey = (resource: IInsomniaExportResource): string =>
    Normalizer.GetSortKeyByType(resource._type) + resource._id;

  private static GetSortKeyByType(type: string): string {
    switch (type) {
      case ExportTypes.Workspace:
        return '0';
      case ExportTypes.ApiSpec:
        return '1';
      case ExportTypes.Environment:
        return '2';
      case ExportTypes.RequestGroup:
        return '3';
      case ExportTypes.Request:
        return '4';
      // this elements should be somewhere at the end
      case ExportTypes.UnitTestSuite:
        return '7';
      case ExportTypes.UnitTest:
        return '8';
      case ExportTypes.CookieJar:
        return '9';
    }
  }

  public static NormalizeImport = (content: IInsomniaExport): string => JSON.stringify(content);
}
