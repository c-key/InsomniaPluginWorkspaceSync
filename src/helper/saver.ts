import { IInsomniaExport, IInsomniaExportResource } from '../common/insomnia';
import { Environment, ExportTypes } from '../common/constants';
import AppError from '../common/app-error';
import Filesystem from './filesystem';

import Path = require('path');

export default class Saver {
  private exportFile: string;
  private folderPath: string;
  private workspaceFile: string;
  private static readonly WorkspaceFilename: string = '_workspace.json';
  private static readonly FileExtension: string = 'json';

  constructor(private insomniaExportPath: string) {
    this.exportFile = Path.join(insomniaExportPath, this.GetFilename('insomnia'));
    this.folderPath = this.exportFile + '-resources';
    this.workspaceFile = Path.join(this.folderPath, Saver.WorkspaceFilename);
  }

  private GetFilename = (name: string) => name.concat('.', Saver.FileExtension);

  private GetResourceIdWithName = (resource: IInsomniaExportResource): string =>
    resource._id + ' | ' + (resource.name || resource.fileName);

  private GetResourceIdFromIdWithName = (resourceIdWithName: string): string => resourceIdWithName.split('|')[0].trim();

  private GetResourcesFiles = async (): Promise<string[]> =>
    (await Filesystem.ReadDirectory(this.folderPath)).filter(file => file != Saver.WorkspaceFilename);

  private GetResourceFilesToRemove = async (resources: IInsomniaExportResource[]): Promise<string[]> =>
    (await this.GetResourcesFiles()).filter(fileName => resources.filter(res => this.GetFilename(res._id) == fileName).length == 0);

  private GetResourceFilename = (resourceId: string) => Path.join(this.folderPath, this.GetFilename(resourceId));

  private async LoadResource(resourceId: string): Promise<IInsomniaExportResource> {
    const resource = await Filesystem.ReadFile<IInsomniaExportResource>(this.GetResourceFilename(resourceId));

    if (resource._type == ExportTypes.Request && resource.body && resource.body.text) {
      resource.body.text = (resource.body.text as any).join(Environment.NewLine);
    }

    return resource;
  }

  private async SaveResource(resource: IInsomniaExportResource): Promise<void> {
    if (resource._type == ExportTypes.Request && resource.body && resource.body.text) {
      resource.body.text = resource.body.text.split(Environment.NewLine) as any;
    }

    Filesystem.WriteFile(this.GetResourceFilename(resource._id), resource);
  }

  public ExportOneFile = async (jsonObject: IInsomniaExport) => Filesystem.WriteFile(this.exportFile, jsonObject);

  public async ExportMultipleFiles(jsonObject: IInsomniaExport): Promise<void> {
    try {
      Filesystem.CreateFolder(this.folderPath);

      jsonObject = JSON.parse(JSON.stringify(jsonObject)); // Why???

      const filesToRemove = await this.GetResourceFilesToRemove(jsonObject.resources as IInsomniaExportResource[]);

      jsonObject.resources.forEach(resource => this.SaveResource(resource));

      filesToRemove.forEach(resourceFile => Filesystem.RemoveFile(Path.join(this.folderPath, resourceFile)));

      jsonObject.resources = jsonObject.resources.map(this.GetResourceIdWithName) as any[];

      Filesystem.WriteFile(this.workspaceFile, jsonObject);
    } catch (e) {
      throw new AppError(`Error during saving of file ${this.workspaceFile}\n${e}`, e);
    }
  }

  public async ImportOneFile(): Promise<IInsomniaExport> {
      try {
        return Filesystem.ReadFile<IInsomniaExport>(this.exportFile);
      } catch (err) {
        throw new AppError(`Error during loading of file ${this.insomniaExportPath}\n${err}`, err);
      }
  }

  public async ImportMultipleFiles(): Promise<IInsomniaExport | null> {
    let jsonObject : IInsomniaExport;
    try {
      jsonObject = await Filesystem.ReadFile<IInsomniaExport>(this.workspaceFile);
    } catch (e) {
      return null;
    }

    try {
      const resourcesIds: string[] = [...jsonObject.resources] as any;
      jsonObject.resources = [];

      for (const resourceIdWithName of resourcesIds) {
          jsonObject.resources.push(await this.LoadResource(this.GetResourceIdFromIdWithName(resourceIdWithName)));
      }

      return jsonObject;
    } catch (err) {
      throw new AppError(`Error during loading of file ${this.workspaceFile}\n${err}`, err);
    }
  }
}
