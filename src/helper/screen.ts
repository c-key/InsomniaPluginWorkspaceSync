import { IInsomniaApp, IInsomniaContext, IInsomniaWorkspace } from '../common/insomnia';
import { Plugin } from '../common/constants';
import ConfigHelper from './config';

import Path = require('path');

export default class ScreenHelper {
  public static ShowAlert = async (app: IInsomniaApp, caption: string, message: string): Promise<void> =>
    app.alert(caption, message);

  public static ShowError = async (app: IInsomniaApp, caption: string, message: string): Promise<void> =>
    ScreenHelper.ShowAlert(app, `ERROR! ${caption}`, message);

  public static async ShowSettings(context: IInsomniaContext, workspace: IInsomniaWorkspace): Promise<void> {
    const configHelper = new ConfigHelper(context.store, workspace._id);

    const savedPath = await configHelper.Get('path') || Path.join(context.app.getPath('desktop'), workspace.name);

    context.app.prompt(`${Plugin.Name} settings`, {
      label: 'Path to synchronize your workspace:',
      defaultValue: savedPath,
      submitName: 'Choose new path',
      cancelable: true,
    }).then(x => {
      context.app.showSaveDialog({ defaultPath: savedPath })
        .then(x => {
          if (x !== null && x !== undefined) {
            const { dir } = Path.parse(Path.resolve(x));
            configHelper.Set('path', dir);
          }
        });
    });
  }
}
