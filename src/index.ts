import { IInsomniaContext, IInsomniaModels } from './common/insomnia';
import { Plugin } from './common/constants';
import WorkSpaceActions from './services/actions';

module.exports.workspaceActions = [
  {
    icon: 'fa-sign-out',
    label: `${Plugin.Name} - Export`,
    action: async (context: IInsomniaContext, models: IInsomniaModels) => WorkSpaceActions.Export(context, models),
  },
  {
    icon: 'fa-sign-in',
    label: `${Plugin.Name} - Import`,
    action: async (context: IInsomniaContext, models: IInsomniaModels) => WorkSpaceActions.Import(context, models),
  },
  {
    icon: 'fa-cog',
    label: `${Plugin.Name} - Configure`,
    action: async (context: IInsomniaContext, models: IInsomniaModels) => WorkSpaceActions.Settings(context, models),
  },
]
