import { IInsomniaContext, IInsomniaExportResource, IInsomniaModels } from '../common/insomnia';
import { ExportTypes } from '../common/constants';
import ConfigHelper from '../helper/config';
import ScreenHelper from '../helper/screen';
import Normalizer from '../helper/normalizer';
import Saver from '../helper/saver';

export default class WorkSpaceActions {
  static async Settings(context: IInsomniaContext, models: IInsomniaModels) {
    ScreenHelper.ShowSettings(context, models.workspace);
  }

  static async Export(context: IInsomniaContext, models: IInsomniaModels) {
    const app = context.app;
    const workspace = models.workspace;
    const config = new ConfigHelper(context.store, workspace._id);

    if (!config.Has('path')) {
      return ScreenHelper.ShowError(app, 'No path configured', 'Please configure the path first.');
    }

    let resourceStats = '';

    try {
      const normalizedExport = Normalizer.NormalizeExport(await context.data.export.insomnia({
        workspace: workspace,
        format: 'json',
        includePrivate: false
      }));

      const workspaceSaver = new Saver(await config.Get('path'));
      await workspaceSaver.ExportOneFile(normalizedExport);
      await workspaceSaver.ExportMultipleFiles(normalizedExport);

      resourceStats = WorkSpaceActions.GenerateResourceStats(normalizedExport.resources);
    } catch (e) {
      return ScreenHelper.ShowError(app, 'Export error', e.message);
    }

    return ScreenHelper.ShowAlert(app, 'Export successful!', `Exported successful: ${resourceStats}`);
  }

  static async Import(context: IInsomniaContext, models: IInsomniaModels) {
    const app = context.app;
    const workspace = models.workspace;
    const config = new ConfigHelper(context.store, workspace._id);

    if (!config.Has('path')) {
      return ScreenHelper.ShowError(app, 'No path configured', 'Please configure the path first.');
    }

    let resourceStats = '';

    try {
      const workspaceSaver = new Saver(await config.Get('path'));
      let normalizedExport = await workspaceSaver.ImportMultipleFiles();
      if (normalizedExport === null) {
        normalizedExport = await workspaceSaver.ImportOneFile();
      }

      if (normalizedExport.resources) {
        const workspace = normalizedExport.resources.find(r => r._type == ExportTypes.Workspace);

        const importOptions = {
          workspaceId: workspace._id,
          workspaceScope: workspace.scope,
        };

        await context.data.import.raw(Normalizer.NormalizeImport(normalizedExport), importOptions);
      } else {
        await context.data.import.raw(Normalizer.NormalizeImport(normalizedExport));
      }

      resourceStats = WorkSpaceActions.GenerateResourceStats(normalizedExport.resources);
    } catch (e) {
      return ScreenHelper.ShowError(app, 'Import error', e.message);
    }
    return ScreenHelper.ShowAlert(app, 'Import successful!', `Imported successful: ${resourceStats}`);
  }

  private static GenerateResourceStats(resources: IInsomniaExportResource[]): string {
    const stats = {};

    resources.forEach(resource => {
      var type = resource._type.replace(/_/g, ' ');

      if (!stats.hasOwnProperty(type)){
        stats[type] = 0;
      }

      stats[type]++;
    });

    return Object.entries(stats).reduce((str, [prop, val]) => {
      if (str.length > 0) str += ', ';
      return `${str}${prop}: ${val}`;
    }, '');
  }
}
